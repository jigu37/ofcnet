<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Node;
use App\Cable;
use App\Ring;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
        return view('home')->with('nodes_count',Node::all()->count())
                           ->with('cables_count',Cable::all()->count())
                           ->with('rings_count',Ring::all()->count());
    }
}
