<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Node;

use App\Cable;

use Session;

use App\RingDetail;

use App\Fibre;

class CableController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('cables.index')->with('cables',Cable::orderBy('slug','asc')->get());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $nodes = Node::orderBy('name','asc')->get();

        return view('cables.create')->with('nodes',$nodes);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)

    {
        $this->validate($request,[

            'from_node_id' => 'required',
            'to_node_id' => 'required',
            'capacity' => 'required',
            
         ]);

        $cable =new Cable;

        $cable->from_node_id = $request->from_node_id;
        
        $cable->to_node_id   = $request->to_node_id;
        
        $cable->capacity     = $request->capacity;
        
        $cable->rkm          = $request->rkm;
        
        $cable->save();

        $ca = Cable::all()->last();

        $from_node = $ca->fromNode->name;
        
        $to_node  = $ca->toNode->name;
        
        $slug      = $from_node."-".$to_node."-".$request->capacity;

        $cable->slug = $slug;

        $cable->save();

        Session::flash('success','Cable created successfully');

        return redirect()->route('cables');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $cables = Cable::where('from_node_id',$id)
                        ->orWhere('to_node_id',$id)
                        ->get();
       
        return view('cables.index')->with('cables',$cables);
                                 
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        $ringdetails = RingDetail::where('cable_id',$id);

        $ringdetails->delete();

        $fibres = Fibre::where('cable_id',$id);

        $fibres->delete();

        $cable = Cable::find($id);

        $cable->delete();

        Session::flash('success','Cable deleted successfully');

        return redirect()->route('cables');


    }
}
