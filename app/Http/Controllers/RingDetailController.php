<?php

namespace App\Http\Controllers;

use App\Cable;
use App\Fibre;
use App\Ring;
use App\RingDetail;
use Illuminate\Http\Request;
use Session;

class RingDetailController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id)
    {
        $ring = Ring::find($id);

        $ringdetails = RingDetail::with(['cable', 'txFibre', 'rxFibre'])->where('ring_id', $id)->get();

        $rLast = RingDetail::with(['cable', 'txFibre', 'rxFibre'])
            ->where('ring_id', $id)
            ->orderBy('updated_at', 'desc')->first();

        if ($ringdetails->count() == 0) {

            $nodeFrom = $ring->fromNode->name;
            $nodeFromId = $ring->fromNode->id;

        } else {

            $nodeFrom = $rLast->toNode->name;
            $nodeFromId = $rLast->toNode->id;

        }

        $nodes1 = Cable::where('from_node_id', $nodeFromId)
            ->distinct()->get(['to_node_id']);
        $nodes2 = Cable::where('to_node_id', $nodeFromId)
            ->distinct()->get(['from_node_id']);

        $cables = Cable::where('from_node_id', $ring->fromNode->id)
            ->orwhere('to_node_id', $ring->fromNode->id)
            ->get();

        return view('ringdetails.create')->with('ring', $ring)
            ->with('cables', $cables)
            ->with('ringdetails', $ringdetails)
            ->with('nodes1', $nodes1)
            ->with('nodes2', $nodes2)
            ->with('nodeFrom', $nodeFrom)
            ->with('nodeFromId', $nodeFromId);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $id)
    {
        $this->validate($request, [

            'cable' => 'required',
            'tx_fibre_no' => 'required',
            'rx_fibre_no' => 'required|different:tx_fibre_no',
            'from_node_id' => 'required',
            'to_node_id' => 'required',
            'slug' => 'required',
        ]);

        $ringdetail = new RingDetail;

        $ringdetail->ring_id = $id;
        $ringdetail->cable_id = $request->cable;
        $ringdetail->tx_fibre_id = $request->tx_fibre_no;
        $ringdetail->rx_fibre_id = $request->rx_fibre_no;
        $ringdetail->from_node_id = $request->from_node_id;
        $ringdetail->to_node_id = $request->to_node_id;
        $ringdetail->slug = $request->slug;

        $ringdetail->save();

        $tx = Fibre::find($request->tx_fibre_no);
        $tx->ring_id = $id;
        $tx->save();

        $rx = Fibre::find($request->rx_fibre_no);
        $rx->ring_id = $id;
        $rx->save();

        Session::flash('success', 'Ring detail added successfully');

        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

        $ringdetails = RingDetail::with(['cable', 'txFibre', 'rxFibre'])->where('ring_id', $id)->get();
        $ring = Ring::find($id);

        return view('ringdetails.show')->with('ringdetails', $ringdetails)
            ->with('ring', $ring);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $r = RingDetail::where('id', $id)->first();
        $fibres = Fibre::where('cable_id', $r->cable_id)
            ->where('ring_id', 0)
            ->get();

        return view('ringdetails.edit')->with('r', $r)
            ->with('fibres', $fibres);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [

            'tx_fibre_no' => 'required',
            'rx_fibre_no' => 'required|different:tx_fibre_no',

        ]);

        $r = RingDetail::find($id);

        $oldtxF = Fibre::find($r->tx_fibre_id);
        $oldtxF->ring_id = 0;
        $oldtxF->save();

        $oldrxF = Fibre::find($r->rx_fibre_id);
        $oldrxF->ring_id = 0;
        $oldrxF->save();

        $r->tx_fibre_id = $request->tx_fibre_no;
        $r->rx_fibre_id = $request->rx_fibre_no;
        $r->save();

        //update fibres table with relevant ring information

        $txF = Fibre::find($request->tx_fibre_no);
        $txF->ring_id = $r->ring_id;
        $txF->save();

        $rxF = Fibre::find($request->rx_fibre_no);
        $rxF->ring_id = $r->ring_id;
        $rxF->save();

        Session::flash('success', 'Fibre detail updated successfully');

        return redirect()->route('rings');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
