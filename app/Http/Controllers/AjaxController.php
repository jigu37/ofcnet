<?php

namespace App\Http\Controllers;

use App\Cable;
use App\Fibre;
use Illuminate\Http\Request;

class AjaxController extends Controller
{

    public function selectAjax(Request $request)
    {

        if ($request->ajax()) {

            $tx_fibres = Fibre::where('cable_id', $request->cable_id)
                ->where('ring_id', '==', 0)->get();
            $datatx = view('ajax.ajax-select-tx')->with('tx_fibres', $tx_fibres)->render();
            $datarx = view('ajax.ajax-select-rx')->with('tx_fibres', $tx_fibres)->render();

            return response()->json(['optionstx' => $datatx, 'optionsrx' => $datarx]);

        }

    }

    public function selectAjax2(Request $request)
    {

        if ($request->ajax()) {

            $from_node_id = $request->from_node_id;
            $to_node_id = $request->to_node_id;

            $cables1 = Cable::where('from_node_id', $from_node_id)
                ->where('to_node_id', $to_node_id)
                ->get();
            $cables2 = Cable::where('from_node_id', $to_node_id)
                ->where('to_node_id', $from_node_id)
                ->get();

            $data = view('ajax.ajax-select2')->with('cables1', $cables1)
                ->with('cables2', $cables2)->render();
            return response()->json(['options' => $data]);
        }

    }
}
