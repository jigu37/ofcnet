<?php

namespace App\Http\Controllers;

use App\Fibre;
use App\Node;
use App\Ring;
use App\RingDetail;
use Illuminate\Http\Request;
use Session;

class RingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('rings.index')->with('rings', Ring::orderBy('name', 'asc')->get());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        $nodes = Node::orderBy('name', 'asc')->get();

        return view('rings.create')->with('nodes', $nodes);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [

            'name' => 'required',
            'suffix' => 'required',
            'from_node_id' => 'required',
            'to_node_id' => 'required|different:from_node_id',
            'slug' => 'required|unique:rings,slug',

        ]);

        $ring = new Ring;

        $ring->name = $request->name;
        $ring->from_node_id = $request->from_node_id;
        $ring->to_node_id = $request->to_node_id;
        $ring->suffix = $request->suffix;
        $ring->slug = $request->slug;

        $ring->save();

        Session::flash('success', 'Ring added successfully');

        return redirect()->route('rings');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $rings = Ring::where('from_node_id', $id)
            ->orWhere('to_node_id', $id)->get();
        return view('rings.index')->with('rings', $rings);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        $fibres = Fibre::where('ring_id', $id)->update(['ring_id' => 0]);

        $ringdetail = RingDetail::where('ring_id', $id);

        $ringdetail->delete();

        $ring = Ring::find($id);

        $ring->delete();

        Session::flash('success', 'Ring deleted successfully');

        return redirect()->route('rings');

    }
    public function __construct()
    {
    }
}
