<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Node;

use Session;

class NodeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       
        $nodes = Node::orderBy('name','asc')->get();
        return view('nodes.index')->with('nodes',$nodes);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('nodes.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[

            'name' => 'required'

        ]);

        $node = new Node;

        $node->name = $request->name;

        $node->save();
        
        Session::flash('success','Node created successfully');

        return redirect()->route('nodes');



    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Node  $node
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Node  $node
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $node = Node::find($id);
        return view('nodes.edit')->with('node',$node);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Node  $node
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[

            'name' => 'required'
        ]);

        $node = Node::find($id);

        $node->name = $request->name;

        $node->save();

        Session::flash('success','Node updated successfully');

        return redirect()->route('nodes');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Node  $node
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $node = Node::find($id);

        $node->delete();

        Session::flash('success','Node deleted successfully');

        return redirect()->route('nodes');
    }
}
