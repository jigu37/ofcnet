<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Fibre;

use App\Cable;

class FibreController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id)
    {

        $fibres = Fibre::with('ring')->where('cable_id',$id)->paginate(12);
        $f = Fibre::all()->where('cable_id',$id)->first();
        $cable = $f->cable->slug;
        return view('fibres.index')->with('fibres',$fibres)
                                   ->with('cable',$cable);
                                   

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store($id,$capacity)
    {
        

        if($capacity > 24)
        {
            $ribbons = $capacity/12;

            $i=1;
                for($j=1; $j<=$ribbons; $j++)
                {

                    for($k=1; $k<=12; $k++)
                    {

                        $fibre = new Fibre;

                        $fibre->cable_id = $id;
            
                        $fibre->fibre_no = $i; $i++;

                        $fibre->slug = '1'."-".$j."-".$k; 

                        $fibre->save();

                    }


                }
        }

        else
        {

            for($j=1; $j<=$capacity; $j++)
            {
                        $fibre = new Fibre;
                        
                        $fibre->cable_id = $id;
                        
                        $fibre->fibre_no = $j;
                        
                        $fibre->slug = $j; 
                        
                        $fibre->save();

            }


        }


        $cable = Cable::find($id);

        $cable->isFibreCreated = 1;

        $cable->save();
       

        return redirect()->back();
          

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
