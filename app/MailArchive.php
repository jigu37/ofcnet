<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MailArchive extends Model
{
    protected $table = 'mails';
}
