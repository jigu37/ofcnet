<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MailAttachment extends Model
{
    protected $table = 'mail_attachments';
}
