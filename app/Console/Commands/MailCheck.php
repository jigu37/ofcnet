<?php

namespace App\Console;

use App\MailArchive;
use Illuminate\Console\Command;
use PhpImap;

class MailCheck extends Command
{

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'mail:check';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Check IMAP mails and sync.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $mailbox = new PhpImap\Mailbox('{imap.gmail.com:993/imap/ssl/novalidate-cert}INBOX', 'ofccentralatd@gmail.com', 'ofccentral@123');

        $mailIds = $mailbox->searchMailBox('UNSEEN');
        if (!$mailIds) {
            $this->info("No mail!");
        } else {
            foreach ($mailIds as $mailId) {
                if (MailArchive::where("mid", $mailId)->count() == 0) {
                    $getMail = $mailbox->getMail($mailId);

                    $tos = [];
                    foreach ($getMail->to as $toMail => $toName) {
                        $tos[] = $toName . "(" . $toMail . ")";
                    }
                    $ccs = [];
                    foreach ($getMail->cc as $ccMail => $ccName) {
                        $ccs[] = $ccName . "(" . $ccMail . ")";
                    }
                    $replys = [];
                    foreach ($getMail->replyTo as $replyMail => $replyName) {
                        $replys[] = $replyName . "(" . $replyMail . ")";
                    }

                    $mail = new MailArchive;
                    $mail->mid = $getMail->id;
                    $mail->date = $getMail->date;
                    $mail->subject = $getMail->subject;
                    $mail->fromName = $getMail->fromName;
                    $mail->fromAddress = $getMail->fromAddress;
                    $mail->to = implode(",", $tos);
                    $mail->cc = implode(",", $ccs);
                    $mail->reply = implode(",", $replys);
                    $mail->plain = $getMail->textPlain;
                    $mail->html = $getMail->textHtml;
                    $mail->save();
                    /*
                    foreach ($getMail->getAttachments() as $getAttachment) {
                    $attachment = new MailAttachment();
                    $attachment->aid = $getAttachment->id;
                    $attachment->name = $getAttachment->name;
                    $attachment->path = $getAttachment->filePath;
                    $attachment->mail_id = $mail->id;
                    $attachment->save();
                    }
                     */
                    $this->info("New Mail:" . $mail->subject);
                }
            }
        }
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return array(
        );
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return array(
        );
    }

}
