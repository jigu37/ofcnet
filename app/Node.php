<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Node extends Model
{
    protected $fillable = ['name'];

    public function cableFrom()
    {
    	return $this->hasMany('App\Cable','from_node_id');
    }

    public function cableTo()
    {
    	return $this->hasMany('App\Cable','to_node_id');
    }


    public function ringFrom()
    {

    	return $this->hasMany('App\Ring','from_node_id');

    }

    public function ringTo()
    {
    	return $this->hasMany('App\Ring','to_node_id');
    }


    public function ringDetailFrom()
    {
        return $this->hasMany('App\RingDetail','from_node_id');
    }

    public function ringDetailTo()
    {
        return $this->hasMany('App\RingDetail','to_node_id');
    }

}
