<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Fibre extends Model
{
    protected $fillable = [

    	'cable_id','fibre_no','slug','remark'

    ];

/**
 * Fibre belongs to particulat Cable
 * @return [type] [description]
 */
    public function cable()
    {
		return $this->belongsTo('App\Cable');
    }

    public function txRingdetail()
    {
    	return $this->belongsTo('App\RingDetail','tx_fibre_id');
    }

     public function rxRingdetail()
    {
    	return $this->belongsTo('App\RingDetail','rx_fibre_id');
    }

    public function ring()
    {
        return $this->belongsTo('App\Ring');
    }


}
