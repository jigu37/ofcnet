<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ring extends Model
{
    protected $fillable = [

    	'name','from_node_id','to_node_id','suffix','slug','isRingBuilt'

    ];

    public function fromNode()
    {

    	return $this->belongsTo('App\Node','from_node_id');
    }

    public function toNode()
    {

    	return $this->belongsTo('App\Node','to_node_id');
    }

    public function ringDetails()

    {
        return $this->hasMany('App\RingDetail');
    }

     public function fibres()
    {
        return $this->hasMany('App\Fibre');
    }
    
}
