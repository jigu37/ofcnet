<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RingDetail extends Model
{
    public function ring()
    {
    	return $this->belongsTo('App\Ring');
    }

    public function cable()
    {
    	return $this->belongsTo('App\Cable');
    }

    public function txFibre()
    {
    	return $this->hasOne('App\Fibre','id','tx_fibre_id');
    }

    public function rxFibre()
    {
        return $this->hasOne('App\Fibre','id','rx_fibre_id');
    }

    public function fromNode()
    {
        return $this->belongsTo('App\Node','from_node_id');
    }

    public function toNode()
    {
        return $this->belongsTo('App\Node','to_node_id');
    }


}
