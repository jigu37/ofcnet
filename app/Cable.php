<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cable extends Model
{
    protected $fillable = [

    	'from_node_id','to_node_id','slug','capacity'

    ];

    public function fromNode()
    {
    	return $this->belongsTo('App\Node','from_node_id');
    }

    public function toNode()
    {
    	return $this->belongsTo('App\Node','to_node_id');
    }

/**
 * [Cable has many fibres]
 * @return [type] [description]
 */
    public function fibres()
    {
        return $this->hasMany('App\Fibre');
    }

    public function ringdetails()
    {
        return $this->hasMany('App\RingDetail');
    }
    
}
