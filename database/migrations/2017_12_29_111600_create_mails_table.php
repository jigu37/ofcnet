<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mails', function (Blueprint $table) {
            $table->increments('id');
            $table->bigInteger("mid");
            $table->dateTime("date");
            $table->string("subject")->nullable();
            $table->string("fromName")->nullable();
            $table->string("fromAddress")->nullable();
            $table->string("to")->nullable();
            $table->string("cc")->nullable();
            $table->string("reply")->nullable();
            $table->text("plain")->nullable();
            $table->text("html")->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('mails');
    }
}
