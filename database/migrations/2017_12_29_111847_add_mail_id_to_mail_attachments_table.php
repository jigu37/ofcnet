<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddMailIdToMailAttachmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('mail_attachments', function (Blueprint $table) {
            $table->integer("mail_id")->unsigned();
            $table->foreign("mail_id")->references("id")->on("mails")->onDelete("cascade");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('mail_attachments', function (Blueprint $table) {
            $table->dropForeign("mail_attachments_mail_id_foreign");
            $table->dropColumn("mail_id");
        });
    }
}
