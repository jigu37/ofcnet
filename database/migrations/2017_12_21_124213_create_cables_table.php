<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCablesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cables', function (Blueprint $table) {

            $table->increments('id');

            $table->integer('from_node_id')->unsigned();

            $table->integer('to_node_id')->unsigned();

            $table->foreign('from_node_id')->references('id')->on('nodes');

            $table->foreign('to_node_id')->references('id')->on('nodes');

            $table->integer('capacity');

            $table->string('rkm')->nullable();

            $table->string('slug')->nullable();

            $table->timestamps();
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cables');
    }
}
