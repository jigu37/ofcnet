<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rings', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->integer('from_node_id')->unsigned();
            $table->integer('to_node_id')->unsigned();
            $table->foreign('from_node_id')->references('id')->on('nodes');
            $table->foreign('to_node_id')->references('id')->on('nodes');
            $table->string('suffix');
            $table->string('slug');
            $table->boolean('isRingBuilt')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rings');
    }
}
