<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnRingDetailTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('ring_details', function (Blueprint $table) {

            $table->string('slug');
            $table->integer('from_node_id')->unsigned();
            $table->integer('to_node_id')->unsigned();
            $table->foreign('from_node_id')->references('id')->on('nodes');
            $table->foreign('to_node_id')->references('id')->on('nodes');
            //
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('ring_details', function (Blueprint $table) {
            //
        });
    }
}
