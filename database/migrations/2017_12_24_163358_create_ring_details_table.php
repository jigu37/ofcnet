<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRingDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ring_details', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ring_id')->unsigned();
            $table->foreign('ring_id')->references('id')->on('rings');
            $table->integer('cable_id')->unsigned();
            $table->foreign('cable_id')->references('id')->on('cables');
            $table->integer('tx_fibre_id')->unsigned();
            $table->integer('rx_fibre_id')->unsigned();
            $table->foreign('tx_fibre_id')->references('id')->on('fibres');
            $table->foreign('rx_fibre_id')->references('id')->on('fibres');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ring_details');
    }
}
