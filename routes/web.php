<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::group(['middleware' => 'auth'], function () {

    Route::get('/home', 'HomeController@index')->name('home');

    /**
     * Node routes
     */
    Route::get('/nodes', 'NodeController@index')->name('nodes');
    Route::get('/node/create', 'NodeController@create')->name('node.create');
    Route::post('/node/store', 'NodeController@store')->name('node.store');
    Route::get('/node/edit/{id}', 'NodeController@edit')->name('node.edit');
    Route::post('/node/update/{id}', 'NodeController@update')->name('node.update');
    Route::get('/node/destroy/{id}', 'NodeController@destroy')->name('node.destroy');

    /**
     * Cable routes
     */
    Route::get('/cables', 'CableController@index')->name('cables');
    Route::get('/cable/create', 'CableController@create')->name('cable.create');
    Route::post('/cable/store', 'CableController@store')->name('cable.store');
    Route::get('cable/show/{id}', 'CableController@show')->name('cable.show');
    Route::get('cable/destroy/{id}', 'CableController@destroy')->name('cable.destroy');

    /**
     * Fibre routes
     */

    Route::get('/fibres/{id}', 'FibreController@index')->name('fibres');
    Route::get('/fibre/store/{id}/{capacity}', 'FibreController@store')->name('fibre.store');

    /**
     * Ring routes
     */
    Route::get('/rings', 'RingController@index')->name('rings');
    Route::get('/ring/create', 'RingController@create')->name('ring.create');
    Route::post('/ring/store', 'RingController@store')->name('ring.store');
    Route::get('/ring/show/{id}', 'RingController@show')->name('ring.show');
    Route::get('/ring/destroy/{id}', 'RingController@destroy')->name('ring.destroy');

    /**
     * Ring Detail routes
     */

    Route::get('/ringdetail/create/{id}', 'RingDetailController@create')->name('ringdetail.create');
    Route::post('/ringdetail/store/{id}', 'RingDetailController@store')->name('ringdetail.store');
    Route::get('/ringdetail/show/{id}', 'RingDetailController@show')->name('ringdetail.show');
    Route::get('/ringdetail/edit/{id}', 'RingDetailController@edit')->name('ringdetail.edit');
    Route::post('/ringdetail/update/{id}', 'RingDetailController@update')->name('ringdetail.update');

    /**
     * Ajax routes for refreshing based on selection of cable
     */

    Route::get('select-ajax', 'AjaxController@selectAjax')->name('select-ajax');
    Route::get('select-ajax2', 'AjaxController@selectAjax2')->name('select-ajax2');

    /**
     * Views controller routes
     */
    Route::get('node-cable/{id}', 'ViewController@nodeCable')->name('node-cable');
    Route::get('node-ring/{id}', 'ViewController@nodeRing')->name('node-ring');

});
