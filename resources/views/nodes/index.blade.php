@extends('layouts.app')

@section('content')
            
            
            
            <div class="panel panel-default">
                <div class="panel-heading">
                    Nodes
                    <a href="{{ route('node.create') }}" class="btn btn-success btn-xs pull-right">Create Node<a>
                </div>
               
                <div class="panel-body">
                    
                    <div class="table-condensed">
                        <table class="table table-responsive">
                            <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Name</th>
                                        <th>Cables</th>
                                        <th>Rings</th>
                                        <th>Edit</th>
                                        <th>Delete</th>
                                    </tr>
                            </thead>
                            <tbody>
                                @if($nodes->count() > 0)
                                    @foreach($nodes as $node)
                                    <tr>
                                        <td>{{ $loop->iteration }}</td>
                                        <td>{{ $node->name }}</td>
                                        <td><a href="{{ route('cable.show',['id' => $node->id]) }}" class="btn btn-xs btn-warning">cables</a></td>
                                        <td><a href="{{ route('ring.show',['id' => $node->id]) }}" class="btn btn-xs btn-primary">rings</a></td>
                                        <td><a href="{{ route('node.edit',['id' => $node->id]) }}" class="btn btn-xs btn-warning">Edit</a></td>
                                        <td><a href="{{ route('node.destroy',['id' => $node->id]) }} " class="btn btn-xs btn-danger">Delete</a></td>
                                    </tr>
                                    @endforeach
                                @else
                                    <tr>
                                        <td colspan="4" class="text-center">No nodes created</td>
                                    </tr>
                                @endif

                            </tbody>
                        </table>
                    </div>
                    
                </div>
            </div>

            
            
 
@endsection
