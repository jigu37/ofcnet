@extends('layouts.app')

@section('content')
            
     @include('includes.errors')

	

	<div class="panel panel-default">

		<div class="panel-heading">
			<h3 class="panel-title">Create new Node</h3>
		</div>

		<div class="panel-body">

			<form action="{{ route('node.store') }}" method="post">

				{{ csrf_field() }}

				<div class="form-group">
					<label for="name">Name</label>
					<input type="text" name="name" class="form-control">
				</div>

				

				<div class="form-group">
					<div class="text-center">
						<button type="submit" class="btn btn-success">Store Node</button>
					</div>
				</div>

			</form>

		</div>

	</div>
 
@endsection
