@extends('layouts.app')

@section('content')
            
     @include('includes.errors')

	

	<div class="panel panel-default">

		<div class="panel-heading">
			<h3 class="panel-title"><h4>{{ $r->ring->slug }}</h4></h3>
		</div>

		<div class="panel-body">

			<form action="{{ route('ringdetail.update',['id'=>$r->id]) }}" method="post">

				{{ csrf_field() }}

				<div class="form-group">
					<label for="cable">Cable</label>
					<input type="text" id="cable" name="cable" class="form-control" value="{{ $r->cable->slug}}" readonly="true">
				</div>

				<div class="form-group">
					<label for    ="tx_fibre_no">Tx Fibre No</label>
					<select name  ="tx_fibre_no" id="tx_fibre_no" class="form-control">
						<option value="{{ $r->txFibre->id }}">{{ $r->txFibre->slug }}</option>
						@foreach($fibres as $fibre)
							
								<option value="{{$fibre->id}}">{{$fibre->slug}}</option>
							
						@endforeach
					</select>
				</div>

				<div class    ="form-group">
					<label for    ="rx_fibre_no">Rx Fibre No</label>
					<select name  ="rx_fibre_no" id="rx_fibre_no" class="form-control">
						<option value="{{ $r->rxFibre->id }}">{{ $r->rxFibre->slug }}</option> 
						@foreach($fibres as $fibre)
							
								<option value="{{$fibre->id}}">{{$fibre->slug}}</option>
							
						@endforeach
					</select>
				</div>

				<div class="form-group">
					<div class="text-center">
						<button type="submit" class="btn btn-success">Update</button>
					</div>
				</div>

			</form>

		</div>


	</div>
 
@endsection


