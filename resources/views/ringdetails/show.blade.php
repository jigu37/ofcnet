@extends('layouts.app')

@section('content')
            
     @include('includes.errors')

	

	<div class="panel panel-default">

		<div class="panel-heading">
			<h3 class="panel-title">
				<h4>
					{{ $ring->slug }}
					<a href="{{ route('ringdetail.create',['id' => $ring->id]) }} " class="btn btn-xs btn-warning pull-right">Built</a>

				</h4> 

			</h3>
		</div>

		<div class="table-responsive">
			<table class="table table-hover">
				<thead>
					<tr>
						<th>#</th>
						<th>Cable</th>
						<th>TxFibre</th>
						<th>RxFibre</th>
						<th>Edit</th>
					</tr>
				</thead>
				<tbody>
				@if($ringdetails->count() > 0)
					@foreach($ringdetails as $r)
					<tr>
						<td>{{ $loop->iteration }}</td>
						<td>{{ $r->slug }}</td>
						<td>{{ $r->txFibre->slug }}</td>
						<td>{{ $r->rxFibre->slug }}</td>
						<td><a href="{{ route('ringdetail.edit',['id' => $r->id])}}" class="btn btn-xs btn-warning">Edit</td>
						<td></td>
						<td></td>
					</tr>
					@endforeach
				@else
					<tr>
						<td colspan="5" class="text-center"> No details available</td>
					</tr>
				@endif

				</tbody>
			</table>
		</div>

				
	</div>

				<div class="form-group">
					<div class="text-center">
						<a href="{{ route('rings') }}" class="btn btn-primary btn-sm">Back</a>
					</div>
				</div>
 
@endsection

@section('scripts')

	<script type="text/javascript">

		$(function(){

	        		setInterval(function(){

	        			var c = $('#cable option:selected').text();
	        			var cap = c.split("-");
	        			var capacity = cap[2];
	        			
	        			var slug = $('#from_node_name').val() + '-' +  $('#to_node_id option:selected').text()
	        						+ '-' + capacity;
        				$('#slug').val(slug);

	        		},500);
				})

		$("select[name='to_node_id']").change(function(){
  			
  			var from_node_id = $("#from_node_id").val();
     		var to_node_id = $(this).val();
      		var token = $("input[name='_token']").val();
      		
      		$.ajax({
          			url: "<?php echo route('select-ajax2'); ?>",
          			method: 'GET',
          			data: { to_node_id:to_node_id, from_node_id:from_node_id },
          			success: function(data) {
          				
            			$("select[name='cable']").html('');
            			$("select[name='cable']").html(data.options);
            			
          			}
      		});
  		});

	
  		$("select[name='cable']").change(function(){
  			//alert('hi');
     		var cable_id = $(this).val();
     		//alert(cable_id);
      		var token = $("input[name='_token']").val();
      		//alert(token);
      		$.ajax({
          			url: "<?php echo route('select-ajax'); ?>",
          			method: 'GET',
          			data: { cable_id: cable_id },
          			success: function(data) {
            			$("select[name='tx_fibre_no']").html('');
            			$("select[name='tx_fibre_no']").html(data.optionstx);
            			$("select[name='rx_fibre_no']").html('');
            			$("select[name='rx_fibre_no']").html(data.optionsrx);
          			}
      		});
  		});

	</script>

@endsection
