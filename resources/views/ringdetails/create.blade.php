@extends('layouts.app')

@section('content')
            
     @include('includes.errors')

	

	<div class="panel panel-default">

		<div class="panel-heading">
			<h3 class="panel-title"><h4>{{ $ring->slug }} </h4></h3>
		</div>

		<div class="panel-body">

			<form action="{{ route('ringdetail.store',['id' => $ring->id ]) }}" method="post">

				{{ csrf_field() }}

				<div class="form-group">
					<label for="from_node_id">From</label>
					<input type="text" id="from_node_name" name="from_node_name" class="form-control" value="{{ $nodeFrom }}" readonly="true">
					<input type="hidden" id="from_node_id" name="from_node_id" class="form-control" value="{{ $nodeFromId }}">

					<label for="to_node_id">To</label>
					<select name="to_node_id" id="to_node_id" class="form-control">
						<option value="">--- Choose ---</option>
						@foreach($nodes1 as $node1)
							<option value="{{ $node1->toNode->id }}">{{ $node1->toNode->name }}</option>
						@endforeach
						@foreach($nodes2 as $node2)
							<option value="{{ $node2->fromNode->id }}">{{ $node2->fromNode->name }}</option>
						@endforeach
					</select>


					<label for="cable">Choose cable</label>
					<select name="cable" id="cable" class="form-control">
						
					</select>
				</div>

				<div class="form-group">
					<label for    ="tx_fibre_no">Tx Fibre No</label>
					<select name  ="tx_fibre_no" id="tx_fibre_no" class="form-control">

					</select>
				</div>

				<div class    ="form-group">
					<label for    ="rx_fibre_no">Rx Fibre No</label>
					<select name  ="rx_fibre_no" id="rx_fibre_no" class="form-control">

					</select>
				</div>

				<div class    ="form-group">
					<label for="slug">Slug</label>
					<input type="text" id="slug" name="slug" class="form-control" value="#" readonly="true">
				</div>

				<div class="form-group">
					<div class="text-center">
						<button type="submit" class="btn btn-success">Store Ring Detail</button>
					</div>
				</div>

			</form>

		</div>

		<div class="table-responsive">
			<table class="table table-hover">
				<thead>
					<tr>
						<th>#</th>
						<th>Cable</th>
						<th>TxFibre</th>
						<th>RxFibre</th>
					</tr>
				</thead>
				<tbody>
				@if($ringdetails->count() > 0)
					@foreach($ringdetails as $r)
					<tr>
						<td>{{ $loop->iteration }}</td>
						<td>{{ $r->slug }}</td>
						<td>{{ $r->txFibre->slug }}</td>
						<td>{{ $r->rxFibre->slug }}</td>
						<td></td>
						<td></td>
					</tr>
					@endforeach
				@else
					<tr>
						<td colspan="5" class="text-center"> No details available</td>
					</tr>
				@endif

				</tbody>
			</table>
		</div>

	</div>
 
@endsection

@section('scripts')

	<script type="text/javascript">

		$(function(){

	        		setInterval(function(){

	        			var c = $('#cable option:selected').text();
	        			var cap = c.split("-");
	        			var capacity = cap[2];
	        			
	        			var slug = $('#from_node_name').val() + '-' +  $('#to_node_id option:selected').text()
	        						+ '-' + capacity;
        				$('#slug').val(slug);

	        		},500);
				})

		$("select[name='to_node_id']").change(function(){
  			
  			var from_node_id = $("#from_node_id").val();
     		var to_node_id = $(this).val();
      		var token = $("input[name='_token']").val();
      		
      		$.ajax({
          			url: "<?php echo route('select-ajax2'); ?>",
          			method: 'GET',
          			data: { to_node_id:to_node_id, from_node_id:from_node_id },
          			success: function(data) {
          				
            			$("select[name='cable']").html('');
            			$("select[name='cable']").html(data.options);
            			
          			}
      		});
  		});

	
  		$("select[name='cable']").change(function(){
  			//alert('hi');
     		var cable_id = $(this).val();
     		//alert(cable_id);
      		var token = $("input[name='_token']").val();
      		//alert(token);
      		$.ajax({
          			url: "<?php echo route('select-ajax'); ?>",
          			method: 'GET',
          			data: { cable_id: cable_id },
          			success: function(data) {
            			$("select[name='tx_fibre_no']").html('');
            			$("select[name='tx_fibre_no']").html(data.optionstx);
            			$("select[name='rx_fibre_no']").html('');
            			$("select[name='rx_fibre_no']").html(data.optionsrx);
          			}
      		});
  		});

	</script>

@endsection
