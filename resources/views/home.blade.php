@extends('layouts.app')

@section('content')

        <div class="col-lg-4 col-md-4 col-sm-4">
            <div class="panel panel-info">
                <div class="panel-heading text-center">
                    Nodes
                </div>
                <div class="panel-body">
                    <h1 class="text-center"><a href="{{ route('nodes') }}">{{ $nodes_count }}</a></h1>

                </div>
                
            </div>
        </div>

        <div class="col-lg-4 col-md-4 col-sm-4">
            <div class="panel panel-warning">
                <div class="panel-heading text-center">
                    Cables
                </div>
                <div class="panel-body">
                    <h1 class="text-center"><a href="{{route('cables')}}">{{ $cables_count }}</a></h1>
                </div>
            </div>
        </div>

        <div class="col-lg-4 col-md-4 col-sm-4">
            <div class="panel panel-danger">
                <div class="panel-heading text-center">
                    Rings
                </div>
                <div class="panel-body">
                    <h1 class="text-center"><a href="{{route('rings')}}">{{ $rings_count }}</a></h1>
                </div>
            </div>
        </div>
                
@endsection
