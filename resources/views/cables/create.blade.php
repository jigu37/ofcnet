@extends('layouts.app')

@section('content')
            
     @include('includes.errors')

	

	<div class="panel panel-default">

		<div class="panel-heading">
			<h3 class="panel-title">Create new cable</h3>
		</div>

		<div class="panel-body">

			<form action="{{ route('cable.store') }}" method="post">

				{{ csrf_field() }}

				<div class="form-group">
					<label for="from_node_id">From Node</label>
					<select name="from_node_id" id="input" class="form-control">
						@foreach($nodes as $fromNode)
							<option value="{{ $fromNode->id }}">{{ $fromNode->name }}</option>
						@endforeach
					</select>
				</div>

				<div class="form-group">
					<label for="to_node_id">To Node</label>
					<select name="to_node_id" id="input" class="form-control">
						@foreach($nodes as $toNode)
							<option value="{{$toNode->id}}">{{ $toNode->name }}</option>
						@endforeach
					</select>
				</div>

				<div class="form-group">
					<label for="capacity">Capacity</label>
					<select name="capacity" id="input" class="form-control">

						<option value="4">4</option>
						<option value="6">6</option>
						<option value="8">8</option>
						<option value="12">12</option>
						<option value="24">24</option>
						<option value="48">48</option>
						<option value="96">96</option>
						<option value="144">144</option>
						<option value="288">288</option>

					</select>
				</div>

				<div class="form-group">
					<label for="rkm">Rkm</label>
					<input type="text" name="rkm" class="form-control">
				</div>


				<div class="form-group">
					<div class="text-center">
						<button type="submit" class="btn btn-success">Store Cable</button>
					</div>
				</div>

			</form>

		</div>

	</div>
 
@endsection
