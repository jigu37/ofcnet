@extends('layouts.app')

@section('content')
            
  <div class="panel panel-default">
                <div class="panel-heading">
                    Cables
                    <a href="{{ route('cable.create') }}" class="btn btn-success btn-xs pull-right">Create cable<a>
                </div>
               
                <div class="panel-body">
                    
                    <div class="table-condensed">
                        <table class="table table-responsive">
                            <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Name</th>
                                        <th>Rkm</th>
                                        <th>Fibre Detail</th>
                                       
                                        <th>Delete</th>
                                    </tr>
                            </thead>
                            <tbody>
                                @if($cables->count() > 0)
                                    @foreach($cables as $cable)
                                    <tr>
                                        <td>{{ $loop->iteration }}</td>
                                        <td>{{ $cable->slug }}</td>
                                        <td>{{ $cable->rkm }}</td>
                                        @if($cable->isFibreCreated)
                                            <td><a href="{{route('fibres',['id' => $cable->id])}} " class="btn btn-xs btn-default">View</a></td>
                                        @else
                                            <td><a href="{{ route('fibre.store',['id' => $cable->id, 'capacity' => $cable->capacity]) }}" class="btn btn-xs btn-warning">Create</a></td>
                                        @endif

                                        <td><a href="{{ route('cable.destroy',['id' => $cable->id]) }}" class="btn btn-xs btn-danger">Delete</a></td>
                                    </tr>
                                    @endforeach
                                @else
                                    <tr>
                                        <td colspan="4" class="text-center">No cable created</td>
                                    </tr>
                                @endif

                            </tbody>
                        </table>
                    </div>
                    
                </div>
            </div>
           
 
@endsection
