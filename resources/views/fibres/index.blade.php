@extends('layouts.app')

@section('content')
            
  <div class="panel panel-default">
                <div class="panel-heading">
                    <h4> {{ $cable }} </h4>
                    
                </div>
               
                <div class="panel-body">
                    
                    <div class="table-condensed">
                        <table class="table table-responsive">
                            <thead>
                                    <tr>
                                        
                                        <th>Fibre No</th>
                                        <th>Circuit</th>
                                     
                                        
                                    </tr>
                            </thead>
                            <tbody>
                                @if($fibres->count() > 0)
                                    @foreach($fibres as $fibre)
                                    <tr>
                                        
                                        <td>{{ $fibre->slug }}</td>
                                        
                                        @if(isset($fibre->ring['slug']))
                                        <td>{{ $fibre->ring['slug'] }}</td> 
                                        @else
                                        <td>spare</td>
                                        @endif
                                                                        
                                        
                                    </tr>
                                    @endforeach
                                @else
                                    <tr>
                                        <td colspan="4" class="text-center">No cable created</td>
                                    </tr>
                                @endif

                            </tbody>

                        </table>
                    </div>
                 <div class="text-center"> {{ $fibres->links() }} </div>  
                </div>
            </div>
           
 
@endsection
