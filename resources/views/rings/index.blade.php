@extends('layouts.app')

@section('content')
            
  <div class="panel panel-default">
                <div class="panel-heading">
                    Rings
                    <a href="{{ route('ring.create') }}" class="btn btn-success btn-xs pull-right">Create ring<a>
                </div>
               
                <div class="panel-body">
                    
                    <div class="table-condensed">
                        <table class="table table-responsive">
                            <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Name</th>  
                                        <th>Delete</th>
                                    </tr>
                            </thead>
                            <tbody>
                                @if($rings->count() > 0)
                                    @foreach($rings as $ring)
                                    <tr>
                                        <td>{{ $loop->iteration }}</td>
                                        <td><a href="{{ route('ringdetail.show',['id' => $ring->id]) }}">{{ $ring->slug }}</a></td>  
                                        <td><a href="{{ route('ring.destroy',['id' => $ring->id]) }}" class="btn btn-xs btn-danger">Delete</a></td>
                                    </tr>
                                    @endforeach
                                @else
                                    <tr>
                                        <td colspan="4" class="text-center">No rings created</td>
                                    </tr>
                                @endif

                            </tbody>
                        </table>
                    </div>
                    
                </div>
            </div>
           
 
@endsection
