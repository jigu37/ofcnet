@extends('layouts.app')

@section('content')
            
     @include('includes.errors')

	

	<div class="panel panel-default">

		<div class="panel-heading">
			<h3 class="panel-title">Create new ring</h3>
		</div>

		<div class="panel-body">

			<form action="{{ route('ring.store') }}" method="post">

				{{ csrf_field() }}

				<div class="form-group">
					<label for="name">Name</label>
					<input type="text" name="name" class="form-control" id="name" value="{{old('name')}}">
				</div>

				<div class="form-group">
					<label for="from_node_id">From (system node)</label>
					<select name="from_node_id" id="fromNode" class="form-control">
						@foreach($nodes as $fromNode)
							<option value="{{ $fromNode->id }}">{{ $fromNode->name }}</option>
						@endforeach
					</select>
				</div>

				<div class="form-group">
					<label for="to_node_id">To (system node)</label>
					<select name="to_node_id" id="toNode" class="form-control">
						@foreach($nodes as $toNode)
							<option value="{{$toNode->id}}">{{ $toNode->name }}</option>
						@endforeach
					</select>
				</div>

				<div class="form-group">
					<label for="suffix">Suffix (eg: Port1 or via VTV)</label>
					<input type="text" name="suffix" class="form-control" id="suffix" value="{{old('suffix')}}">
				</div>

				<div class="form-group">
					<label for="slug">Slug</label>
					<input type="text" name="slug" id="slug" class="form-control" readonly="true">
				</div>



				<div class="form-group">
					<div class="text-center">
						<button type="submit" class="btn btn-success">Store Ring</button>
					</div>
				</div>

			</form>

		</div>

	</div>
 
@endsection

@section('scripts')
	
	<script>
	        	$(function(){

	        		setInterval(function(){

	        			var slug = $('#name').val()+ '-' + $('#fromNode option:selected').text() + '-' +  $('#toNode option:selected').text() + '-' + $('#suffix').val();
        				$('#slug').val(slug);

	        		},500);
				})

	</script>
@endsection
